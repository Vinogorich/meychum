"use strict";
document.addEventListener("DOMContentLoaded", () => {
  let catalogOpenBtn = document.querySelector(".mch-header__catalog-btn");
  let catalogMenuFirstWrapper = document.querySelector(
    ".mch-catalog-first-wrapper"
  );
  let catalogMenuMainContainer = document.querySelector(".mch-menu__item-list");
  let catalogMenuItemsArray = catalogMenuMainContainer.querySelectorAll(
    ".mch-menu__item"
  );
  let mchCloseCatalog = catalogMenuMainContainer.querySelector(
    ".mch-item-content__btn-close"
  );
  let mchOverlay = document.querySelector(".mch-menu-overlay");
  //reusable methods
  function mchRemoveClass(whatRemove, whereRemove) {
    for (let j = 0; j < whereRemove.length; j++) {
      whereRemove[j].classList.remove(whatRemove);
    }
  }
  function mchClick(whatListen, method) {
    whatListen.addEventListener("click", method);
  }
  function gridSize() {
    const SMALL = 399;
    const MED = 767;
    const GRID = {
      small: 1,
      med: 2,
      large: 3,
    };
    return window.innerWidth <= SMALL
      ? GRID.small
      : window.innerWidth <= MED
      ? GRID.med
      : GRID.large;
  }
  //toggle catalog
  function mchToggleCatalog() {
    catalogMenuFirstWrapper.classList.toggle("visibleFirstLevel");
    catalogOpenBtn.classList.toggle("mch-header__catalog--open");
    mchOverlay.classList.toggle("mch-open");
  }

  //close catalog second-level
  mchCloseCatalog.addEventListener("click", () => {
    catalogMenuFirstWrapper.classList.remove("mch-show-first-wrapper");
    mchRemoveClass("mch-show__item-content", catalogMenuItemsArray);
  });
  //open on hover list item in each list catalog
  for (let i = 0; i < catalogMenuItemsArray.length; i++) {
    catalogMenuItemsArray[i].addEventListener("mouseover", () => {
      catalogMenuFirstWrapper.classList.add("mch-show-first-wrapper");
      mchRemoveClass("mch-show__item-content", catalogMenuItemsArray);
      catalogMenuItemsArray[i].classList.add("mch-show__item-content");
    });
  }
  //change button catalog
  function mchCatalogMobile() {}
  mchClick(catalogOpenBtn, mchToggleCatalog);
  mchClick(mchOverlay, mchToggleCatalog);
  window.addEventListener("resize", function () {
    // scripts
  });
});
